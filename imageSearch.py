# [imageSearch.py]
# author: Kristin Hamilton
# last modified: 30-Mar-2017

import pathlib
import re
import subprocess
import inputValidation
import sqlite3

"""Methods responsible for determining whether a given image satisfies the user-provided 
criteria, including 'search term expansion'/synset subtree lookups
"""


def image_search(directory_path, src_terms):
    """Main function for image classification portion of Cura; returns list of all images in 
    directory found to contain object specified by user-provided search terms
    """
    
    result_imgs = []
    predicted_cats_for_img = []
    
    directory_imgs = inputValidation.get_valid_directory_imgs(directory_path)
    expanded_term_list = get_expanded_term_list(src_terms)
    
    # Run each img in directory against classifier to obtain list of predicted categories
    for img in directory_imgs:
        predicted_cats_for_img = get_predictions_inception_v3(img)
        
        for cat in predicted_cats_for_img:
            if cat in expanded_term_list:
                result_imgs.append(img.name)
                break
                        
    print(">>> Image search complete")
    return result_imgs


def get_expanded_term_list(src_terms):
    """Build list of all possible terms that would satisfy the search terms (all terms 
    appearing in subtree of each respective search term)
    """
    
    subtree_query = ""
    expanded_term_list = []
    
    srcTerm_synsetID_list = get_srcTerm_synsetID_list(src_terms)
    range_pairs = get_subtree_synsetID_range_pairs(srcTerm_synsetID_list)
    
    subtree_query += "SELECT DISTINCT word FROM synset WHERE synsetID BETWEEN '" + range_pairs[0][0] + "' AND '" + range_pairs[0][1] + "'"
    
    for pair in range_pairs[1:]:
        subtree_query += " OR synsetID BETWEEN '" + pair[0] + "' AND '" + pair[1] + "'"

    db = sqlite3.connect('./db/cura.db') # @UndefinedVariable
    c = db.cursor()  
        
    for row in c.execute(subtree_query):
        expanded_term_list.extend(row)

    print(len(expanded_term_list))
    return expanded_term_list    


def get_srcTerm_synsetID_list(srcTerms):
    """Build list of synsetIDs, where the synset corresponding to a given synsetID contains 
    an instance of a given search term; returns set of all synsetIDs found for all search 
    terms
    """
    
    id_query = ""
    srcTerm_synsetID_list = []
    
    id_query += "SELECT synsetID FROM synset WHERE word = '" + srcTerms[0] + "'";
    
    for term in srcTerms[1:]:
        id_query += " OR word = '" + term + "'"

    db = sqlite3.connect('./db/cura.db') # @UndefinedVariable
    c = db.cursor()
    
    for row in c.execute(id_query):
        srcTerm_synsetID_list.extend(row)
    
    return srcTerm_synsetID_list


def get_subtree_synsetID_range_pairs(srcTerm_synsetID_list):
    """Get synsetID ranges for subtree associated with each synsetID, each corresponding to 
    a search term
    """
    
    range_query = ""
    range_pairs = []
    
    range_query += "SELECT range_start, range_end FROM synset_subtree_range WHERE synsetID = '" + srcTerm_synsetID_list[0] + "'"
    
    for synsetID in srcTerm_synsetID_list[1:]:
        range_query += " OR synsetID = '" + synsetID + "'"

    db = sqlite3.connect('./db/cura.db') # @UndefinedVariable
    c = db.cursor() 
        
    range_pairs.extend(c.execute(range_query))    
    
    return range_pairs


def get_predictions_inception_v3(file):
    """Get predicted categories for image using Google's Inception v3 model; parse response/
    strip unneeded data and characters, and return as trimmed list"""
    
    filepath = pathlib.PurePath(file)
    filename_arg = '--image_file=' + str(filepath)
    
    completed_ps = subprocess.run(['python3', 'classify_image.py', filename_arg], 
                              stdout=subprocess.PIPE, 
                              universal_newlines=True)
    predictions = completed_ps.stdout
    regex_ptrn = '\\n|, | \('
    split_preds = re.split(regex_ptrn, predictions)
    prediction_list = []
    
    for word in split_preds:
        if(not word.startswith('score')):
            prediction_list.append(word)
            
    return prediction_list

