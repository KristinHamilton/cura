# Cura


### Third-party code

* `Cura/classify_image.py` (tensorflow/models on GitHub; Apache 2.0 license; script for running images against pre-trained Inception v3 model)
* `Cura/curaGuiDesign/*` (Qt Creator files; cross compiled to Python to produce curaGuiDesign.py)
  * *Qt licensing: GNU GPL v3*


### General description

Cura is a cross platform desktop application that leverages image recognition to allow users to search for images by keyword. Cura is written primarily in Python, while the GUI was written using Qt 5, and has an SQLite database containing references that emulate a hierarchy of synsets or classification labels. A pre-trained model from Google's Inception project provides the object recognition component.

The objective of Cura is to provide an intuitive interface and functionality that allows users to offload the often extremely tedious and time-consuming process of manual organization of a collection of images.

Cura requires no internet connection to operate. Statements of current and future work, along with screenshots illustrating the current state of the application are provided below.


### Screenshots

![Directory selection](./readme_imgs/cura_img1.png) 
![Search term input and search result](./readme_imgs/cura_img2.png)


### Current work

* Refactoring (done for the time being)
* Packaging and release of installers for major desktop platforms (Mac, Linux, Windows)


### Future work

* Adding more structure between synsets to support robustness of relationships represented
* Optimizations (speed, etc.)
* Additional features: Accessibility-focused
* Implementing filesystem behaviors (i.e. drag-and-drop actions, etc., allowing GUI to behave more like desktop file manager--including adding in sorting capability for result images)
    
