# [main.py]
# author: Kristin Hamilton
# last modified: 28-Nov-2016

"""Code for initial CLI-based proof-of-concept version of Cura application"""


import os
import pathlib
import curaUtils
import fileUtils
import searchTermUtils

validFileExtensions = fileUtils.get_valid_image_file_extensions()  #['.jpg', '.jpeg', '.JPG', '.JPEG']  
img_files = []
searchTermFileList = []
searchTermFileListMap = dict()
searchTermSynonymList = []
searchTermSynonymListMap = dict()

curaUtils.print_program_startup_info()
directoryPath = input('Enter directoryPath: ')

try:
    directoryFiles = os.listdir(directoryPath) # for debugging: /home/kristin/seuProjects/cosc4157/testImages
except:
    try:
        directoryFiles = [directoryPath]
    except:
        exit('Error: Path ' + directoryPath + ' does not exist. Exiting program.')

searchTerms = input('Enter search term(s): ').split(', ')
# TODO: check at least one search term entered
# TODO: check at least one of the search terms is available in list of labels

userInput_searchRelatedTerms = input('Expand search to related terms? (y/n) ')
searchRelatedTerms = userInput_searchRelatedTerms.capitalize() in ('Y', 'YES', 'T', 'TRUE')

# check for valid file extensions
for file in directoryFiles:
    fileExtensions = pathlib.PurePath(file).suffixes
    for fileExtension in fileExtensions:
        if str(fileExtension) in validFileExtensions:
            absoluteFilePath = pathlib.PurePath(directoryPath).joinpath(pathlib.PurePath(file))
            img_files.append(absoluteFilePath)
# initialize synonym and file maps
for searchTerm in searchTerms:
    if searchRelatedTerms:
        searchTermSynonymListMap[searchTerm] = searchTermUtils.get_synonyms(searchTerm)
    else:
        searchTermSynonymListMap[searchTerm] = []
    
    searchTermFileListMap[searchTerm] = []

# search for objects in image files  
for file in img_files:
    for searchTerm in searchTerms:
        searchTermSynonymList = searchTermSynonymListMap[searchTerm]
        if curaUtils.find_object_in_image(file, searchTerm, searchTermSynonymList):
            searchTermFileListMap[searchTerm].append(file.name)

print()
print('Results for ' + directoryPath + ':')
for term in searchTerms:
    print('  > Search term: ' + term)
    
    if searchRelatedTerms:
        print('      Related terms:', end = ' ')
        searchTermSynonymList = searchTermSynonymListMap[term]
        if len(searchTermSynonymList) > 0:
            listLen = len(searchTermSynonymList)
            for i in range(listLen):
                if i < listLen - 1:
                    print(searchTermSynonymList[i], end = ', ')
                else:
                    print(searchTermSynonymList[i])
        else:
            print('(No related terms found)')
                
    searchTermFileList = searchTermFileListMap[term]
    if len(searchTermFileList) <= 0:
        print('      0 results for ' + term)
    else:
        for file in searchTermFileList:
            print('      ' + str(file))
