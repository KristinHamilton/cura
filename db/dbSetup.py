# [dbSetup.py]
# author: Kristin Hamilton
# last modified: 30-Mar-2017

"""Script for (re)creating database"""


import sqlite3

db = sqlite3.connect('cura.db') # @UndefinedVariable
c = db.cursor()

# Delete existing db contents
c.execute("DROP TABLE IF EXISTS synset_subtree_range;")
c.execute("DROP TABLE IF EXISTS synset;")

# Create tables
c.execute("CREATE TABLE synset (" +
            "synsetID TEXT," + 
            "word TEXT," +
            "PRIMARY KEY(synsetID, word)" +
        ");")

c.execute("CREATE TABLE synset_subtree_range (" +
            "synsetID TEXT NOT NULL," + 
            "range_start TEXT NOT NULL," +
            "range_end TEXT NOT NULL," +
            "PRIMARY KEY(synsetID, range_start, range_end)" + 
            "FOREIGN KEY(synsetID) REFERENCES synset(synsetID)," +
            "FOREIGN KEY(range_start) REFERENCES synset(synsetID)," +
            "FOREIGN KEY(range_end) REFERENCES synset(synsetID)" +
        ");")

# Populate tables
sqlScript = open('populate-synset.sql', 'r').read()
c.executescript(sqlScript)

sqlScript = open('populate-synset_subtree_range.sql', 'r').read()
c.executescript(sqlScript)
