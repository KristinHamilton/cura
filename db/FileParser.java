import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException; 
import java.io.PrintWriter;

// parse file: /home/kristin/seuProjects/cosc4157/Cura/model/imagenet_synset_to_human_label_map.txt

public class FileParser{
    
    public static void main(String[] args){
        String inputFilename = "/home/kristin/seuProjects/cosc4157/Cura/model/imagenet_synset_to_human_label_map.txt";
        String outputFilename = "/home/kristin/seuProjects/cosc4157/SynsetFileParser/parsed_synset_file.csv";
        
        File inputFile = new File(inputFilename);
        File outputFile = new File(outputFilename);
        
        Scanner sc = null;
        PrintWriter pw = null;
        
        try{
            sc = new Scanner(inputFile);
        }
        catch(FileNotFoundException e){
            System.out.println("Error: input file " + inputFilename + " not found");
        }
        
        StringBuilder sb = new StringBuilder();
        String synsetID = "";
        String[] synsetTerms;
        
        while(sc.hasNextLine()){
            synsetID = sc.next();
            synsetTerms = sc.nextLine().split(", ");
            
            for(String term : synsetTerms){
                sb.append(synsetID);
                sb.append(",");
                sb.append(term.trim());
                sb.append("\n");
            }
        }
        
        sc.close();
        
        outputFile = new File(outputFilename);
        
        try{
            pw = new PrintWriter(outputFile);
            pw.print(sb.toString());
        }
        catch(FileNotFoundException e){
            System.out.println("Error: output file " + outputFilename + " not found");
        }
        
        pw.close();
        System.out.println(">>> Success");        
    }
}
