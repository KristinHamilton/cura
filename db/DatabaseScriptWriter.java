import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class DatabaseScriptWriter {

    public static void main(String[] args){
        String csvFilename = "";
        String destFilename = "";
        String tableName = "";
        String populateScript = "";
        
        // table: synset
        csvFilename = "/home/kristin/seuProjects/cosc4157/Cura/db/synset.csv";
        destFilename = "/home/kristin/seuProjects/cosc4157/Cura/db/populate-synset.sql";
        tableName = "synset";
        populateScript = generatePopulateScriptFromCsvFile(tableName, csvFilename);
        writeScriptToFile(populateScript, destFilename);
        System.out.println("Script has been written to " + destFilename);
        
        // table: synset_subtree_range
        csvFilename = "/home/kristin/seuProjects/cosc4157/Cura/db/synset_subtree_range.csv";
        destFilename = "/home/kristin/seuProjects/cosc4157/Cura/db/populate-synset_subtree_range.sql";
        tableName = "synset_subtree_range";
        populateScript = generatePopulateScriptFromCsvFile(tableName, csvFilename);
        writeScriptToFile(populateScript, destFilename);
        System.out.println("Script has been written to " + destFilename);
    }
    
    public static String generatePopulateScriptFromCsvFile(String tableName, String csvFilename){
        StringBuilder sb = new StringBuilder();
        File file1 = new File(csvFilename);
        Scanner sc = null;
        
        try{
            sc = new Scanner(file1);
        }
        catch(FileNotFoundException e){
            System.out.println("Error: srcFile " + csvFilename + " not found");
        }
        
        String[] fields;
        sb.append("INSERT INTO " + tableName + " VALUES\n");
        
        while(sc.hasNextLine()){
            fields = sc.nextLine().split(",");
            
            sb.append("  (");
            for(String field : fields)
                sb.append("\"" + field + "\",");            
            sb.deleteCharAt(sb.length() - 1);
            sb.append(")");
            
            if(sc.hasNextLine())
                sb.append(",\n");
            else
                sb.append(";");
        }
        
        sc.close();
        return sb.toString();
    }
    
    public static void writeScriptToFile(String script, String destFilename){
        File file1 = new File(destFilename);
        PrintWriter pw = null;
        
        try{
            pw = new PrintWriter(file1);
        }
        catch(FileNotFoundException e){
            System.out.println("Error: destFile " + destFilename + " not found");
        }
        
        pw.println(script);
        pw.close();        
    }
}
