# [inputValidation.py]
# author: Kristin Hamilton
# last modified: 30-Mar-2017

import os
import pathlib
import sqlite3

"""Methods for performing validation of user-provided inputs."""


def get_valid_directory_imgs(directory_path):
    """Generate list of files in user-provided directory that satisfy current supported 
    filetypes
    """

    directory_imgs = []
    directory_files = []
    file_extensions = []
    
    try:
        directory_files = os.listdir(directory_path)
    except:
        try:
            directory_files = [directory_path]
        except:
            exit('Error: Path ' + directory_path + ' does not exist. Exiting program.')

    valid_file_extensions = get_valid_image_file_extensions()

    # check for valid file extensions
    for file in directory_files:
        file_extensions = pathlib.PurePath(file).suffixes
        for file_extension in file_extensions:
            if str(file_extension) in valid_file_extensions:
                absolute_file_path = pathlib.PurePath(directory_path).joinpath(pathlib.PurePath(file))
                directory_imgs.append(absolute_file_path)
                
    return directory_imgs


def validate_search_terms(src_terms):
    """Confirm at least one of the user-provided search terms appears at least once in db"""

    src_terms_as_string = "', '".join(str(s) for s in src_terms)
    validation_query = "SELECT COUNT(*) FROM synset WHERE word IN ('" + src_terms_as_string + "');"
    
    db = sqlite3.connect('./db/cura.db') # @UndefinedVariable
    c = db.cursor()
    
    for row in c.execute(validation_query):
        count = row[0]

    return count > 0


def get_valid_image_file_extensions():
    """Returns list of raster image file format extensions; hardcoded for the time being"""
    
    return ['.jpg', '.jpeg', '.JPG', '.JPEG']

    '''
    validFileExtensions = [
                           '.BMP', '.DIB', '.bmp', '.dib',                    # bitmap
                           '.JIF', '.JFI', '.JFIF', '.JPE', '.JPEG', '.JPG',  # JPEG
                           '.jif', '.jfi', '.jfif', '.jpe', '.jpeg', '.jpg', 
                           '.J2K', '.JP2', '.JPF', '.JPM', '.JPX', '.MJ2',    # JPEG-2000
                           '.j2k', '.jp2', '.jpf', '.jpm', '.jpx', '.mj2', 
                           '.PNG', '.TIF', '.TIFF', '.png', '.tif', '.tiff',  # PNG, TIFF
                           '.BPG', '.WEBP', '.bpg', '.webp',  # newer image file formats
                           '.PBM', '.PGM', '.PPM', '.PNM',    # Netpbm formats
                           '.pbm', '.pgm', '.ppm', '.pnm'
                           ]
    
    return validFileExtensions
    '''

