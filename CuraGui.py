# [CuraGui.py]
# author: Kristin Hamilton
# last modified: 30-Mar-2017

import sys
import os
import pathlib
from curaGuiDesign import curaGuiDesign
from PyQt5 import QtCore, QtGui, QtWidgets
import inputValidation
import imageSearch


class CuraGui(QtWidgets.QMainWindow, curaGuiDesign.Ui_MainWindow):
    """Main logic for Cura GUI; action definitions for element events, etc."""
    
    
    def __init__(self):
        super(self.__class__, self).__init__()
        
        self.directory = None
        self.search_terms = []
        
        self.setupUi(self)
        self.setWindowTitle("Cura")
        
        self.listWidget.setViewMode(QtWidgets.QListWidget.IconMode)
        self.listWidget.setIconSize(QtCore.QSize(200, 200))
        self.listWidget.setResizeMode(QtWidgets.QListWidget.Adjust)
        
        self.pushButton_2.clicked.connect(self.browse_folder)
        self.pushButton.clicked.connect(self.get_search_results)


    def browse_folder(self):
        self.listWidget.clear()
        self.directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Select a folder")
        
        if self.directory:
            for file_name in os.listdir(self.directory):
                absolute_file_path = pathlib.PurePath(self.directory).joinpath(pathlib.PurePath(file_name))
                file_item = QtWidgets.QListWidgetItem(QtGui.QIcon(str(absolute_file_path)), 
                                                      file_name, None, QtWidgets.QListWidgetItem.Type)
                self.listWidget.addItem(file_item)


    def get_search_results(self):
        self.search_terms = self.textEdit.toPlainText().split(', ')
        
        if inputValidation.validate_search_terms(self.search_terms):
            image_list = imageSearch.image_search(self.directory, self.search_terms)
            self.listWidget.clear()
            
            for file_name in image_list:
                    absolute_file_path = pathlib.PurePath(self.directory).joinpath(pathlib.PurePath(file_name))
                    file_item = QtWidgets.QListWidgetItem(QtGui.QIcon(str(absolute_file_path)), 
                                                          file_name, None, QtWidgets.QListWidgetItem.Type)
                    self.listWidget.addItem(file_item)    
        else:
            self.textEdit.setText("Search term not found.")


def launchGui():
    app = QtWidgets.QApplication(sys.argv)
    form = CuraGui()
    form.show()
    app.exec_()


if __name__ == '__main__':
    launchGui()

